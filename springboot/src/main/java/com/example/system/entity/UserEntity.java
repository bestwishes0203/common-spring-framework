package com.example.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author he
 * @since 2024-03-23
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_user")
@Schema(name = "UserEntity", description = "$!{table.comment}")
public class UserEntity extends Model<UserEntity> {

    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "姓名")
    @TableField("user_name")
    private String name;

    @Schema(description = "性别")
    @TableField("user_sex")
    private String sex;

    @Schema(description = "年龄")
    @TableField("user_age")
    private Integer age;

    @Schema(description = "头像")
    @TableField("user_pic")
    private String pic;

    @Schema(description = "账号")
    @TableField("user_acc")
    private String acc;

    @Schema(description = "密码")
    @TableField("user_pwd")
    private String pwd;

    @Schema(description = "电话号码")
    @TableField("user_phone")
    private String phone;

    @Schema(description = "电子邮件")
    @TableField("user_email")
    private String email;

    @Schema(description = "用户部门")
    @TableField("user_dept")
    private String dept;

    @Schema(description = "用户岗位")
    @TableField("user_post")
    private String post;

    @Schema(description = "状态（0正常 1停用）")
    @TableField("status")
    private String status;

    @Schema(description = "创建者")
    @TableField("create_by")
    private String createBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(description = "更新者")
    @TableField("update_by")
    private String updateBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description = "更新时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}
