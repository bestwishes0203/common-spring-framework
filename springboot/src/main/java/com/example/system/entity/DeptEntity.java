package com.example.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 部门岗位表
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_dept")
@Schema(name = "DeptEntity", description = "$!{table.comment}")
public class DeptEntity extends Model<DeptEntity> {

    @Schema(description = "编号")
    @TableId(value = "dept_id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "名称")
    @TableField("dept_name")
    private String name;

    @Schema(description = "排序")
    @TableField("dept_ranks")
    private Integer ranks;

    @Schema(description = "类别")
    @TableField("dept_sort")
    private Integer sort;

    @Schema(description = "标识符")
    @TableField("dept_parent")
    private Integer parent;

    @Schema(description = "状态（0正常 1停用）")
    @TableField("status")
    private String status;

    @Schema(description = "创建者")
    @TableField("create_by")
    private String createBy;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(description = "更新者")
    @TableField("update_by")
    private String updateBy;

    @Schema(description = "更新时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}
