package com.example.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_dict")
@Schema(name = "DictEntity", description = "$!{table.comment}")
public class DictEntity extends Model<DictEntity> {

    @Schema(description = "编号")
    @TableId(value = "dict_id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "名称")
    @TableField("dict_name")
    private String name;

    @Schema(description = "字段")
    @TableField("dict_field")
    private String field;

    @Schema(description = "数据")
    @TableField("dict_data")
    private String data;

    @Schema(description = "含义")
    @TableField("dict_mean")
    private String mean;

    @Schema(description = "状态（0正常 1停用）")
    @TableField("status")
    private String status;

    @Schema(description = "创建者")
    @TableField("create_by")
    private String createBy;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(description = "更新者")
    @TableField("update_by")
    private String updateBy;

    @Schema(description = "更新时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}
