package com.example.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 岗位表
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_post")
@Schema(name = "PostEntity", description = "$!{table.comment}")
public class PostEntity extends Model<PostEntity> {

    @Schema(description = "编号")
    @TableId(value = "post_id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "名称")
    @TableField("post_name")
    private String name;

    @Schema(description = "编码")
    @TableField("post_code")
    private String code;

    @Schema(description = "排序")
    @TableField("post_ranks")
    private Integer ranks;

    @Schema(description = "状态（0正常 1停用）")
    @TableField("status")
    private String status;

    @Schema(description = "创建者")
    @TableField("create_by")
    private String createBy;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(description = "更新者")
    @TableField("update_by")
    private String updateBy;

    @Schema(description = "更新时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}
