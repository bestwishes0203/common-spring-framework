package com.example.system.service.impl;

import com.example.system.entity.RoleEntity;
import com.example.system.mapper.RoleMapper;
import com.example.system.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, RoleEntity> implements RoleService {

}
