package com.example.system.service;

import com.example.system.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author he
 * @since 2024-03-23
 */
public interface UserService extends IService<UserEntity> {

}
