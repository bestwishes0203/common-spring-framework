package com.example.system.service.impl;

import com.example.system.entity.MenuEntity;
import com.example.system.mapper.MenuMapper;
import com.example.system.service.MenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, MenuEntity> implements MenuService {

}
