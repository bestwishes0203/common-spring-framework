package com.example.system.service.impl;

import com.example.system.entity.PostEntity;
import com.example.system.mapper.PostMapper;
import com.example.system.service.PostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 岗位表 服务实现类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Service
public class PostServiceImpl extends ServiceImpl<PostMapper, PostEntity> implements PostService {

}
