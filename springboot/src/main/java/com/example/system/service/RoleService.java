package com.example.system.service;

import com.example.system.entity.RoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
public interface RoleService extends IService<RoleEntity> {

}
