package com.example.system.service.impl;

import com.example.system.entity.DictEntity;
import com.example.system.mapper.DictMapper;
import com.example.system.service.DictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, DictEntity> implements DictService {

}
