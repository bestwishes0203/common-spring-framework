package com.example.system.service;

import com.example.system.entity.DictEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
public interface DictService extends IService<DictEntity> {

}
