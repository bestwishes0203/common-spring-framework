package com.example.system.service;

import com.example.system.entity.DeptEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 部门岗位表 服务类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
public interface DeptService extends IService<DeptEntity> {

}
