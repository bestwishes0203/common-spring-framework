package com.example.system.service;

import com.example.system.entity.RoleMenuEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色-菜单表 服务类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
public interface RoleMenuService extends IService<RoleMenuEntity> {

}
