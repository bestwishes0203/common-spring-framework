package com.example.system.service.impl;

import com.example.system.entity.UserRoleEntity;
import com.example.system.mapper.UserRoleMapper;
import com.example.system.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色-菜单表 服务实现类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRoleEntity> implements UserRoleService {

}
