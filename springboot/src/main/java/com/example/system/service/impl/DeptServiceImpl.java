package com.example.system.service.impl;

import com.example.system.entity.DeptEntity;
import com.example.system.mapper.DeptMapper;
import com.example.system.service.DeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门岗位表 服务实现类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, DeptEntity> implements DeptService {

}
