package com.example.system.service;

import com.example.system.entity.PostEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 岗位表 服务类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
public interface PostService extends IService<PostEntity> {

}
