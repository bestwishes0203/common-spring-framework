package com.example.system.service.impl;

import com.example.system.entity.RoleMenuEntity;
import com.example.system.mapper.RoleMenuMapper;
import com.example.system.service.RoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色-菜单表 服务实现类
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenuEntity> implements RoleMenuService {

}
