package com.example.system.mapper;

import com.example.system.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author he
 * @since 2024-03-23
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

}
