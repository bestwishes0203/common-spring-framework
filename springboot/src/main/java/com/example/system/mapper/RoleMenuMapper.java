package com.example.system.mapper;

import com.example.system.entity.RoleMenuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 角色-菜单表 Mapper 接口
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenuEntity> {

}
