package com.example.system.mapper;

import com.example.system.entity.PostEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 岗位表 Mapper 接口
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Mapper
public interface PostMapper extends BaseMapper<PostEntity> {

}
