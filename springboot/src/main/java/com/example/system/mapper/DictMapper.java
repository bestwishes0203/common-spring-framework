package com.example.system.mapper;

import com.example.system.entity.DictEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Mapper
public interface DictMapper extends BaseMapper<DictEntity> {

}
