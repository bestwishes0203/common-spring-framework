package com.example.system.resp;

import com.example.system.entity.*;
import com.example.system.req.MenuParent;
import lombok.Data;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

@Data
public class LoginResp {
    private UserEntity user;
    private UserRoleEntity userRole;
    private RoleEntity role;
    private List<RoleMenuEntity> rmList;
    private ArrayList<Number> menuIdList;
    private ArrayList<MenuEntity> parList;
    private ArrayList<MenuEntity> childList;
    private ArrayList<MenuParent> menuList;
    //private ArrayList<MenuEntity> menuList;
}
