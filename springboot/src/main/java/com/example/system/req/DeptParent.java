package com.example.system.req;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Data
public class DeptParent {

    @Schema(description = "编号")
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "图标")
    @TableField("menu_icon")
    private String icon;

    @Schema(description = "名称")
    @TableField("menu_name")
    private String name;

    @Schema(description = "路径")
    @TableField("menu_path")
    private String path;

    @Schema(description = "组件")
    @TableField("menu_component")
    private String component;

    @Schema(description = "排序")
    @TableField("menu_ranks")
    private Integer ranks;

    @Schema(description = "标识符")
    @TableField("menu_parent")
    private Integer parent;

    @Schema(description = "状态（0正常 1停用）")
    @TableField("status")
    private String status;

    @Schema(description = "创建者")
    @TableField("create_by")
    private String createBy;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Schema(description = "更新者")
    @TableField("update_by")
    private String updateBy;

    @Schema(description = "更新时间")
    @TableField("update_time")
    private Date updateTime;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    @Schema(description = "子元素")
    @TableField("")
    private ArrayList children;

}
