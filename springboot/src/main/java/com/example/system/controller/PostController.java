package com.example.system.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.req.IdParam;
import com.example.common.resp.Result;
import com.example.system.entity.PostEntity;
import com.example.system.mapper.PostMapper;
import com.example.system.service.PostService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 岗位表 前端控制器
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Tag(name = "岗位")
@RestController
@RequestMapping("/postEntity")
public class PostController {

    @Autowired
    private PostMapper postMapper;
    @Autowired
    private PostService postService;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Operation(summary = "列表-分页")
    @PostMapping("/listPage")
    public Result listPagePost(@RequestParam Integer page, @RequestParam Integer pageSize) {
        //分页参数
        Page<PostEntity> rowPage = new Page(page, pageSize);
        //queryWrapper组装查询where条件
        LambdaQueryWrapper<PostEntity> queryWrapper = new LambdaQueryWrapper<>();
        rowPage = postMapper.selectPage(rowPage, queryWrapper);
        return Result.success("数据分页列表", rowPage);
    }

    @Operation(summary = "查询-名称")
    @PostMapping("/selectPostByName/{param}")
    public Result selectPostByName(@PathVariable @Validated String param) {
        QueryWrapper<PostEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("post_name"), param);
        if (postService.count(wrapper) == 0) return Result.failure("字典名称不存在", sdf.format(new Date()));
        return Result.success(postService.getOne(wrapper));
    }

    @Operation(summary = "查询-编号")
    @PostMapping("/selectPost/{param}")
    public Result selectPost(@PathVariable @Validated Integer param) {
        QueryWrapper<PostEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("post_id"), param);
        if (postService.count(wrapper) == 0) return Result.failure("字典编号不存在", sdf.format(new Date()));
        return Result.success(postService.getOne(wrapper));
    }

    @Operation(summary = "保存")
    @PostMapping("/insert")
    public Result insertPost(@RequestBody @Validated PostEntity param) {
        QueryWrapper<PostEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("post_id"), param.getId());
        if (postService.count(wrapper) != 0) return Result.failure("编号已存在", sdf.format(new Date()));
        if (postService.save(param)) return Result.success("保存成功", sdf.format(new Date()));
        return Result.failure("保存失败", sdf.format(new Date()));
    }

    @Operation(summary = "删除")
    @PostMapping("/delete")
    public Result deletePost(@RequestBody @Validated IdParam param) {
        QueryWrapper<PostEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("post_id"), param.getId());
        if (postService.count(wrapper) == 0) return Result.failure("编号不存在", param.getId());
        if (postService.remove(wrapper)) return Result.success("删除成功", param.getId());
        return Result.failure("删除失败", param.getId());
    }

    @Operation(summary = "修改")
    @PostMapping("/update")
    public Result updatePost(@RequestBody @Validated PostEntity param) {
        QueryWrapper<PostEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("post_id"), param.getId());
        if (postService.count(wrapper) == 0) return Result.failure("编号不存在", sdf.format(new Date()));
        if (postService.update(wrapper)) return Result.success("修改成功", sdf.format(new Date()));
        return Result.failure("修改失败", sdf.format(new Date()));
    }

    @Operation(summary = "导出数据")
    @PostMapping("exportExcel")
    public void exportExcelPost(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("Excel数据表", StandardCharsets.UTF_8).replaceAll("\\+", "%20");
        List<PostEntity> list = postService.list();
        response.setHeader("Content-disposition", "attachment;filename*=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), PostEntity.class).sheet("Excel数据表").doWrite(list);
    }

    @Operation(summary = "导入数据")
    @PostMapping("/importExcel")
    public Result importExcelPost(MultipartFile file) {
        try {
            //获取文件的输入流
            InputStream inputStream = file.getInputStream();
            List<PostEntity> list = EasyExcel.read(inputStream) //调用read方法
                    //注册自定义监听器，字段校验可以在监听器内实现
                    //.registerReadListener(new PostListener())
                    .head(PostEntity.class) //对应导入的实体类
                    .sheet(0) //导入数据的sheet页编号，0代表第一个sheet页，如果不填，则会导入所有sheet页的数据
                    .headRowNumber(1) //列表头行数，1代表列表头有1行，第二行开始为数据行
                    .doReadSync();//开始读Excel，返回一个List<T>集合，继续后续入库操作
            //模拟导入数据库操作
            for (PostEntity entity : list) {
                postService.saveOrUpdate(entity);
            }
            return Result.success("导入成功", sdf.format(new Date()));
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

}
