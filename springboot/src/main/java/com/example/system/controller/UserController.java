package com.example.system.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.req.IdParam;
import com.example.common.resp.Result;
import com.example.system.entity.UserEntity;
import com.example.system.mapper.UserMapper;
import com.example.system.resp.LoginResp;
import com.example.system.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author he
 * @since 2024-03-23
 */
@Tag(name = "用户")
@RestController
@RequestMapping("/userEntity")
public class UserController {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Operation(summary = "列表-分页")
    @PostMapping("/listPage")
    public Result listPageUser(@RequestParam Integer page, @RequestParam Integer pageSize) {
        //分页参数
        Page<UserEntity> rowPage = new Page(page, pageSize);
        //queryWrapper组装查询where条件
        LambdaQueryWrapper<UserEntity> queryWrapper = new LambdaQueryWrapper<>();
        rowPage = userMapper.selectPage(rowPage, queryWrapper);
        return Result.success("数据列表", rowPage);
    }

    @Operation(summary = "查询-姓名")
    @PostMapping("/selectUserByName/{param}")
    public Result selectUserByName(@PathVariable @Validated String param) {
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("user_name"), param);
        if (userService.count(wrapper) == 0) return Result.failure("用户姓名不存在", sdf.format(new Date()));
        return Result.success(userService.getOne(wrapper));
    }

    @Operation(summary = "查询-账号")
    @PostMapping("/selectUserByAcc/{param}")
    public Result selectUserByAcc(@PathVariable @Validated String param) {
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("user_acc"), param);
        if (userService.count(wrapper) == 0) return Result.failure("账号不存在", sdf.format(new Date()));
        return Result.success(userService.getOne(wrapper));
    }

    @Operation(summary = "保存")
    @PostMapping("/insert")
    public Result insertUser(@RequestBody @Validated UserEntity param) {
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("user_id"), param.getId());
        if (userService.count(wrapper) != 0) return Result.failure("编号已存在", sdf.format(new Date()));
        if (userService.save(param)) return Result.success("保存成功", sdf.format(new Date()));
        return Result.failure("保存失败", sdf.format(new Date()));
    }

    @Operation(summary = "删除")
    @PostMapping("/delete")
    public Result deleteUser(@RequestBody @Validated IdParam param) {
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("user_id"), param.getId());
        if (userService.count(wrapper) == 0) return Result.failure("编号不存在", param.getId());
        if (userService.remove(wrapper)) return Result.success("删除成功", param.getId());
        return Result.failure("删除失败", param.getId());
    }

    @Operation(summary = "修改")
    @PostMapping("/update")
    public Result updateUser(@RequestBody @Validated UserEntity param) {
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("user_id"), param.getId());
        if (userService.count(wrapper) == 0) return Result.failure("用户编号不存在", sdf.format(new Date()));
        if (userService.updateById(param)) return Result.success("修改成功", sdf.format(new Date()));
        return Result.failure("修改失败", sdf.format(new Date()));
    }

    @Operation(summary = "导出数据")
    @PostMapping("exportExcel")
    public void exportExcelUser(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("Excel数据表", StandardCharsets.UTF_8).replaceAll("\\+", "%20");
        List<UserEntity> list = userService.list();
        response.setHeader("Content-disposition", "attachment;filename*=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), UserEntity.class).sheet("Excel数据表").doWrite(list);
    }

    @Operation(summary = "导入数据")
    @PostMapping("/importExcel")
    public Result importExcelUser(MultipartFile file) {
        try {
            //获取文件的输入流
            InputStream inputStream = file.getInputStream();
            List<UserEntity> list = EasyExcel.read(inputStream) //调用read方法
                    //注册自定义监听器，字段校验可以在监听器内实现
                    //.registerReadListener(new UserListener())
                    .head(UserEntity.class) //对应导入的实体类
                    .sheet(0) //导入数据的sheet页编号，0代表第一个sheet页，如果不填，则会导入所有sheet页的数据
                    .headRowNumber(1) //列表头行数，1代表列表头有1行，第二行开始为数据行
                    .doReadSync();//开始读Excel，返回一个List<T>集合，继续后续入库操作
            //模拟导入数据库操作
            for (UserEntity entity : list) {
                userService.saveOrUpdate(entity);
            }
            return Result.success("导入成功", sdf.format(new Date()));
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
