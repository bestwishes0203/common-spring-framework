package com.example.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.common.resp.Result;
import com.example.system.entity.*;
import com.example.system.mapper.*;
import com.example.system.req.MenuParent;
import com.example.system.resp.LoginResp;
import com.example.system.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Tag(name = "登录注册")
@RestController
@RequestMapping("/userEntity")
public class LoginController {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleMenuMapper roleMenuMapper;
    @Autowired
    private RoleMenuService roleMenuService;

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private MenuService menuService;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Operation(summary = "用户-登录")
    @PostMapping("/loginUser")
    public Result loginUser(@RequestParam String acc, @RequestParam String pwd, @RequestParam String code) {
        LoginResp resp = new LoginResp();
        //获取用户信息
        long count = userService.count(new QueryWrapper<UserEntity>().eq("user_acc", acc));
        if (count == 0) return Result.failure("用户账号不存在");
        UserEntity one = userService.getOne(new QueryWrapper<UserEntity>().eq("user_acc", acc));
        if (!Objects.equals(one.getPwd(), pwd)) return Result.failure("用户密码错误");
        resp.setUser(one);
        //用户角色信息
        UserRoleEntity userRole = userRoleService.getOne(new QueryWrapper<UserRoleEntity>().eq("ur_user", one.getId()));
        resp.setUserRole(userRole);
        //角色信息
        RoleEntity role = roleService.getOne(new QueryWrapper<RoleEntity>().eq("role_id", userRole.getRole()));
        resp.setRole(role);
        //角色菜单
        List<RoleMenuEntity> roleMenuList = roleMenuService.list(new QueryWrapper<RoleMenuEntity>().eq("rm_role", role.getId()));
        resp.setRmList(roleMenuList);
        //菜单编号集合
        ArrayList<Number> IdList = new ArrayList<>();
        for (int i = 0; i < roleMenuList.size(); i++) {
            IdList.add(roleMenuList.get(i).getMenu());
        }
        resp.setMenuIdList(IdList);
        //菜单信息
        ArrayList<MenuEntity> parList = new ArrayList<>();
        ArrayList<MenuEntity> childList = new ArrayList<>();
        for (int i = 0; i < IdList.size(); i++) {
            //获取par==0 fu
            MenuEntity parItem = menuService.getOne(new QueryWrapper<MenuEntity>().eq("menu_id", IdList.get(i)).eq("menu_parent", 0));
            if (parItem != null) parList.add(parItem);
            //获取par!=0 zi
            MenuEntity childItem = menuService.getOne(new QueryWrapper<MenuEntity>().eq("menu_id", IdList.get(i)).ne("menu_parent", 0));
            if (childItem != null) childList.add(childItem);
        }
        // 返回fu zi 菜单
        resp.setParList(parList);
        resp.setChildList(childList);
        //处理fu数据
        ArrayList<MenuParent> parents = new ArrayList<>();
        for (int i = 0; i < parList.size(); i++) {
            MenuEntity menu = parList.get(i);
            MenuParent menuParent = new MenuParent();
            BeanUtils.copyProperties(menu, menuParent);
            parents.add(menuParent);
        }
        //处理zi数据
        for (int i = 0; i < parents.size(); i++) {
            ArrayList<MenuEntity> entities = new ArrayList<>();
            for (int j = 0; j < childList.size(); j++) {
                MenuEntity menu = childList.get(j);
                //如果zi元素的par==fu元素id，zi添加到fu child
                if (Objects.equals(parents.get(i).getId(), childList.get(j).getParent())) {
                    entities.add(menu);
                }
            }
            parents.get(i).setChildren(entities);
        }
        resp.setMenuList(parents);
        //返回结果
        return Result.success("登录成功", resp);
    }
}
