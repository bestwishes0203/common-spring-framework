package com.example.system.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.req.IdParam;
import com.example.common.resp.Result;
import com.example.system.entity.MenuEntity;
import com.example.system.req.MenuParent;
import com.example.system.mapper.MenuMapper;
import com.example.system.service.MenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author he
 * @since 2024-04-08
 */
@Tag(name = "菜单")
@RestController
@RequestMapping("/menuEntity")
public class MenuController {

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private MenuService menuService;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Operation(summary = "列表")
    @PostMapping("/list")
    public Result listMenu() {
        //获取par==0
        QueryWrapper<MenuEntity> par = new QueryWrapper<>();
        par.eq("menu_parent", 0);
        List<MenuEntity> listParent = menuService.list(par);
        //获取par!=0
        QueryWrapper<MenuEntity> child = new QueryWrapper<>();
        child.ne("menu_parent", 0);
        List<MenuEntity> listChild = menuService.list(child);
        //处理fu数据
        ArrayList<MenuParent> parents = new ArrayList<>();
        for (int i = 0; i < listParent.size(); i++) {
            MenuEntity menu = listParent.get(i);
            MenuParent menuParent = new MenuParent();
            BeanUtils.copyProperties(menu,menuParent);
            parents.add(menuParent);
        }
        //处理zi数据
        for (int i = 0; i < parents.size(); i++) {
            MenuParent menuParent = parents.get(i);
            Integer id = parents.get(i).getId();
            ArrayList<MenuEntity> entities = new ArrayList<>();
            for (int j = 0; j < listChild.size(); j++) {
                MenuEntity menu = listChild.get(j);
                Integer parent = listChild.get(j).getParent();
                if (Objects.equals(id, parent)) {
                    entities.add(menu);
                }
            }
            menuParent.setChildren(entities);
        }
        return Result.success("数据列表", parents);
    }

    @Operation(summary = "列表-分页")
    @PostMapping("/listPage")
    public Result listPageMenu(@RequestParam Integer page, @RequestParam Integer pageSize) {
        //分页参数
        Page<MenuEntity> rowPage = new Page(page, pageSize);
        //queryWrapper组装查询where条件
        LambdaQueryWrapper<MenuEntity> queryWrapper = new LambdaQueryWrapper<>();
        rowPage = menuMapper.selectPage(rowPage, queryWrapper);
        return Result.success("数据分页列表", rowPage);
    }

    @Operation(summary = "查询-名称")
    @PostMapping("/selectMenuByName/{param}")
    public Result selectMenuByName(@PathVariable @Validated String param) {
        QueryWrapper<MenuEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("menu_name"), param);
        if (menuService.count(wrapper) == 0) return Result.failure("角色名称不存在", sdf.format(new Date()));
        return Result.success(menuService.getOne(wrapper));
    }

    @Operation(summary = "查询-编号")
    @PostMapping("/selectMenu/{param}")
    public Result selectMenu(@PathVariable @Validated Integer param) {
        QueryWrapper<MenuEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("menu_id"), param);
        if (menuService.count(wrapper) == 0) return Result.failure("角色编号不存在", sdf.format(new Date()));
        return Result.success(menuService.getOne(wrapper));
    }

    @Operation(summary = "保存")
    @PostMapping("/insert")
    public Result insertMenu(@RequestBody @Validated MenuEntity param) {
        QueryWrapper<MenuEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("menu_id"), param.getId());
        if (menuService.count(wrapper) != 0) return Result.failure("编号已存在", sdf.format(new Date()));
        if (menuService.save(param)) return Result.success("保存成功", sdf.format(new Date()));
        return Result.failure("保存失败", sdf.format(new Date()));
    }

    @Operation(summary = "删除")
    @PostMapping("/delete")
    public Result deleteMenu(@RequestBody @Validated IdParam param) {
        QueryWrapper<MenuEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("menu_id"), param.getId());
        if (menuService.count(wrapper) == 0) return Result.failure("编号不存在", param.getId());
        if (menuService.remove(wrapper)) return Result.success("删除成功", param.getId());
        return Result.failure("删除失败", param.getId());
    }

    @Operation(summary = "修改")
    @PostMapping("/update")
    public Result updateMenu(@RequestBody @Validated MenuEntity param) {
        QueryWrapper<MenuEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(("menu_id"), param.getId());
        if (menuService.count(wrapper) == 0) return Result.failure("编号不存在", sdf.format(new Date()));
        if (menuService.update(wrapper)) return Result.success("修改成功", sdf.format(new Date()));
        return Result.failure("修改失败", sdf.format(new Date()));
    }

    @Operation(summary = "导出数据")
    @PostMapping("exportExcel")
    public void exportExcelMenu(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("Excel数据表", StandardCharsets.UTF_8).replaceAll("\\+", "%20");
        List<MenuEntity> list = menuService.list();
        response.setHeader("Content-disposition", "attachment;filename*=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), MenuEntity.class).sheet("Excel数据表").doWrite(list);
    }

    @Operation(summary = "导入数据")
    @PostMapping("/importExcel")
    public Result importExcelMenu(MultipartFile file) {
        try {
            //获取文件的输入流
            InputStream inputStream = file.getInputStream();
            List<MenuEntity> list = EasyExcel.read(inputStream) //调用read方法
                    //注册自定义监听器，字段校验可以在监听器内实现
                    //.registerReadListener(new MenuListener())
                    .head(MenuEntity.class) //对应导入的实体类
                    .sheet(0) //导入数据的sheet页编号，0代表第一个sheet页，如果不填，则会导入所有sheet页的数据
                    .headRowNumber(1) //列表头行数，1代表列表头有1行，第二行开始为数据行
                    .doReadSync();//开始读Excel，返回一个List<T>集合，继续后续入库操作
            //模拟导入数据库操作
            for (MenuEntity entity : list) {
                menuService.saveOrUpdate(entity);
            }
            return Result.success("导入成功", sdf.format(new Date()));
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

}
