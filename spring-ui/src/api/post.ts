import request from "@/request";


// 参数接口
export interface UpdatePostParams {
    id?: number;
    name?: string;
    code?: string;
    ranks?: number;
    status?: string;
    createBy?: string;
    createTime?: Record<string, unknown>;
    updateBy?: string;
    updateTime?: Record<string, unknown>;
    remark?: string;
}

// 响应接口
export interface UpdatePostRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 修改
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {string} params.name 名称
 * @param {string} params.code 编码
 * @param {number} params.ranks 排序
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function updatePost(params: UpdatePostParams): Promise<UpdatePostRes> {
    return request.post(`/postEntity/update`, params);
}


// 响应接口
export interface SelectPostByNameRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-名称
 * @param {string} param
 * @returns
 */
export function selectPostByName(param: string): Promise<SelectPostByNameRes> {
    return request.post(`/postEntity/selectPostByName/${param}`);
}

// 响应接口
export interface SelectPostRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-编号
 * @param {string} param
 * @returns
 */
export function selectPost(param: number): Promise<SelectPostRes> {
    return request.post(`/postEntity/selectPost/${param}`);
}

// 响应接口
export interface ListPagePostRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 列表-分页
 * @param {string} page
 * @param {string} pageSize
 * @returns
 */
export function listPagePost(page: number, pageSize: number): Promise<ListPagePostRes> {
    return request.post(`/postEntity/listPage?page=${page}&pageSize=${pageSize}`);
}

// 参数接口
export interface InsertPostParams {
    id?: number;
    name?: string;
    code?: string;
    ranks?: number;
    status?: string;
    createBy?: string;
    createTime?: Record<string, unknown>;
    updateBy?: string;
    updateTime?: Record<string, unknown>;
    remark?: string;
}

// 响应接口
export interface InsertPostRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 保存
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {string} params.name 名称
 * @param {string} params.code 编码
 * @param {number} params.ranks 排序
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function insertPost(params: InsertPostParams): Promise<InsertPostRes> {
    return request.post(`/postEntity/insert`, params);
}

// 响应接口
export interface ImportExcelPostRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 导入数据
 * @param {string} file
 * @returns
 */
export function importExcelPost(file: object): Promise<ImportExcelPostRes> {
    return request.post(`/postEntity/importExcel?file=${file}`);
}


// 响应接口
export interface ExportExcelPostRes {
}

/**
 * 导出数据
 * @returns
 */
export function exportExcelPost(): Promise<ExportExcelPostRes> {
    return request.post(`/postEntity/exportExcel`);
}

// 参数接口
export interface DeletePostParams {
    id?: number;
}

// 响应接口
export interface DeletePostRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 删除
 * @param {object} params IdParam
 * @param {number} params.id ID，记录唯一标识
 * @returns
 */
export function deletePost(params: DeletePostParams): Promise<DeletePostRes> {
    return request.post(`/postEntity/delete`, params);
}

