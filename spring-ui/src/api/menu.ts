import request from "@/request";


// 参数接口
export interface UpdateMenuParams {
    id?: number;
    icon?: string;
    name?: string;
    path?: string;
    component?: string;
    ranks?: number;
    parent?: number;
    status?: string;
    createBy?: string;
    createTime?: string | unknown;
    updateBy?: string;
    updateTime?: string | unknown;
    remark?: string;
}

// 响应接口
export interface UpdateMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 修改
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {string} params.icon 图标
 * @param {string} params.name 名称
 * @param {string} params.path 路径
 * @param {string} params.component 组件
 * @param {number} params.ranks 排序
 * @param {number} params.parent 标识符
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function updateMenu(params: UpdateMenuParams): Promise<UpdateMenuRes> {
    return request.post(`/menuEntity/update`, params);
}

// 响应接口
export interface SelectMenuByNameRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-名称
 * @param {string} param
 * @returns
 */
export function selectMenuByName(param: string): Promise<SelectMenuByNameRes> {
    return request.post(`/menuEntity/selectMenuByName/${param}`);
}

// 响应接口
export interface SelectMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-编号
 * @param {string} param
 * @returns
 */
export function selectMenu(param: number): Promise<SelectMenuRes> {
    return request.post(`/menuEntity/selectMenu/${param}`);
}

// 响应接口
export interface ListMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 列表
 * @returns
 */
export function listMenu(): Promise<ListMenuRes> {
    return request.post(`/menuEntity/list`);
}

// 响应接口
export interface ListPageMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 列表-分页
 * @param {string} page
 * @param {string} pageSize
 * @returns
 */
export function listPageMenu(page: number, pageSize: number): Promise<ListPageMenuRes> {
    return request.post(`/menuEntity/listPage?page=${page}&pageSize=${pageSize}`);
}

// 参数接口
export interface InsertMenuParams {
    id?: number;
    icon?: string;
    name?: string;
    path?: string;
    component?: string;
    ranks?: number;
    parent?: number;
    status?: string;
    createBy?: string;
    createTime?: string | unknown;
    updateBy?: string;
    updateTime?: string | unknown;
    remark?: string;
}

// 响应接口
export interface InsertMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 保存
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {string} params.icon 图标
 * @param {string} params.name 名称
 * @param {string} params.path 路径
 * @param {string} params.component 组件
 * @param {number} params.ranks 排序
 * @param {number} params.parent 标识符
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function insertMenu(params: InsertMenuParams): Promise<InsertMenuRes> {
    return request.post(`/menuEntity/insert`, params);
}

// 响应接口
export interface ImportExcelMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 导入数据
 * @param {string} file
 * @returns
 */
export function importExcelMenu(file: object): Promise<ImportExcelMenuRes> {
    return request.post(`/menuEntity/importExcel?file=${file}`);
}

// 响应接口
export interface ExportExcelMenuRes {
}

/**
 * 导出数据
 * @returns
 */
export function exportExcelMenu(): Promise<ExportExcelMenuRes> {
    return request.post(`/menuEntity/exportExcel`);
}

// 参数接口
export interface DeleteMenuParams {
    id?: number;
}

// 响应接口
export interface DeleteMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 删除
 * @param {object} params IdParam
 * @param {number} params.id ID，记录唯一标识
 * @returns
 */
export function deleteMenu(params: DeleteMenuParams): Promise<DeleteMenuRes> {
    return request.post(`/menuEntity/delete`, params);
}

