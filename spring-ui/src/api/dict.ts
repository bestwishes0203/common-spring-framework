import request from "@/request";


// 参数接口
export interface UpdateDictParams {
    id?: number;
    name?: string;
    field?: string;
    data?: string;
    mean?: string;
    status?: string;
    createBy?: string;
    createTime?: Record<string, unknown>;
    updateBy?: string;
    updateTime?: Record<string, unknown>;
    remark?: string;
}

// 响应接口
export interface UpdateDictRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 修改
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {string} params.name 名称
 * @param {string} params.field 字段
 * @param {string} params.data 数据
 * @param {string} params.mean 含义
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function updateDict(params: UpdateDictParams): Promise<UpdateDictRes> {
    return request.post(`/dictEntity/update`, params);
}

// 响应接口
export interface SelectDictByNameRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-名称
 * @param {string} param
 * @returns
 */
export function selectDictByName(param: string): Promise<SelectDictByNameRes> {
    return request.post(`/dictEntity/selectDictByName/${param}`);
}

// 响应接口
export interface SelectDictRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-编号
 * @param {string} param
 * @returns
 */
export function selectDict(param: number): Promise<SelectDictRes> {
    return request.post(`/dictEntity/selectDict/${param}`);
}



// 响应接口
export interface ListPageDictRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 列表-分页
 * @param {string} page
 * @param {string} pageSize
 * @returns
 */
export function listPageDict(page: number, pageSize: number): Promise<ListPageDictRes> {
    return request.post(`/dictEntity/listPage?page=${page}&pageSize=${pageSize}`);
}

// 参数接口
export interface InsertDictParams {
    id?: number;
    name?: string;
    field?: string;
    data?: string;
    mean?: string;
    status?: string;
    createBy?: string;
    createTime?: Record<string, unknown>;
    updateBy?: string;
    updateTime?: Record<string, unknown>;
    remark?: string;
}

// 响应接口
export interface InsertDictRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 保存
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {string} params.name 名称
 * @param {string} params.field 字段
 * @param {string} params.data 数据
 * @param {string} params.mean 含义
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function insertDict(params: InsertDictParams): Promise<InsertDictRes> {
    return request.post(`/dictEntity/insert`, params);
}

// 响应接口
export interface ImportExcelDictRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 导入数据
 * @param {string} file
 * @returns
 */
export function importExcelDict(file: object): Promise<ImportExcelDictRes> {
    return request.post(`/dictEntity/importExcel?file=${file}`);
}

// 响应接口
export interface ExportExcelDictRes {}

/**
 * 导出数据
 * @returns
 */
export function exportExcelDict(): Promise<ExportExcelDictRes> {
    return request.post(`/dictEntity/exportExcel`);
}

// 参数接口
export interface DeleteDictParams {
    id?: number;
}

// 响应接口
export interface DeleteDictRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 删除
 * @param {object} params IdParam
 * @param {number} params.id ID，记录唯一标识
 * @returns
 */
export function deleteDict(params: DeleteDictParams): Promise<DeleteDictRes> {
    return request.post(`/dictEntity/delete`, params);
}

