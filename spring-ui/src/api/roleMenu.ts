import request from "@/request";

// 参数接口
export interface UpdateRoleMenuParams {
    id?: number;
    role?: number;
    menu?: number;
    status?: string;
    createBy?: string;
    createTime?: Record<string, unknown>;
    updateBy?: string;
    updateTime?: Record<string, unknown>;
    remark?: string;
}

// 响应接口
export interface UpdateRoleMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 修改
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {number} params.role 角色编号
 * @param {number} params.menu 菜单编号
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function updateRoleMenu(params: UpdateRoleMenuParams): Promise<UpdateRoleMenuRes> {
    return request.post(`/roleMenuEntity/update`, params);
}

// 响应接口
export interface SelectRMByRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-角色
 * @param {string} param
 * @returns
 */
export function selectRMByRole(param: number): Promise<SelectRMByRoleRes> {
    return request.post(`/roleMenuEntity/selectRMByRole/${param}`);
}

// 响应接口
export interface SelectRMByMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-菜单
 * @param {string} param
 * @returns
 */
export function selectRMByMenu(param: number): Promise<SelectRMByMenuRes> {
    return request.post(`/roleMenuEntity/selectRMByMenu/${param}`);
}

// 响应接口
export interface ListPageRoleMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 列表-分页
 * @param {string} page
 * @param {string} pageSize
 * @returns
 */
export function listPageRoleMenu(page: number, pageSize: number): Promise<ListPageRoleMenuRes> {
    return request.post(`/roleMenuEntity/listPage?page=${page}&pageSize=${pageSize}`);
}

// 参数接口
export interface InsertRoleMenuParams {
    id?: number;
    role?: number;
    menu?: number;
    status?: string;
    createBy?: string;
    createTime?: Record<string, unknown>;
    updateBy?: string;
    updateTime?: Record<string, unknown>;
    remark?: string;
}

// 响应接口
export interface InsertRoleMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 保存
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {number} params.role 角色编号
 * @param {number} params.menu 菜单编号
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function insertRoleMenu(params: InsertRoleMenuParams): Promise<InsertRoleMenuRes> {
    return request.post(`/roleMenuEntity/insert`, params);
}

// 响应接口
export interface ImportExcelRoleMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 导入数据
 * @param {string} file
 * @returns
 */
export function importExcelRoleMenu(file: object): Promise<ImportExcelRoleMenuRes> {
    return request.post(`/roleMenuEntity/importExcel?file=${file}`);
}

// 响应接口
export interface ExportExcelRoleMenuRes {}

/**
 * 导出数据
 * @returns
 */
export function exportExcelRoleMenu(): Promise<ExportExcelRoleMenuRes> {
    return request.post(`/roleMenuEntity/exportExcel`);
}

// 参数接口
export interface DeleteRoleMenuParams {
    id?: number;
}

// 响应接口
export interface DeleteRoleMenuRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 删除
 * @param {object} params IdParam
 * @param {number} params.id ID，记录唯一标识
 * @returns
 */
export function deleteRoleMenu(params: DeleteRoleMenuParams): Promise<DeleteRoleMenuRes> {
    return request.post(`/roleMenuEntity/delete`, params);
}

