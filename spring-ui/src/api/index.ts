import * as common from '@/api/common'
import * as dept from '@/api/dept'
import * as dict from '@/api/dict'
import * as menu from '@/api/menu'
import * as post from '@/api/post'
import * as role from '@/api/role'
import * as roleMenu from '@/api/roleMenu'
import * as user from '@/api/user'
import * as userRole from '@/api/userRole'

const api = {
    common,
    dept,
    dict,
    menu,
    post,
    role,
    roleMenu,
    user,
    userRole,
}

export default api;


// interface api { home: typeof home; }
// export default { home, } as api;