import request from "@/request";

// 响应接口
export interface GenerateImageCodeRes {
}

/**
 * 生成验证码
 * @returns
 */
export function generateImageCode(): Promise<GenerateImageCodeRes> {
    return request.get(`/generateImageCode`, {
        responseType: "arraybuffer",
    });
}

// 响应接口
export interface UploadRes {}

/**
 * 上传图片到本地
 * @param {string} file
 * @returns
 */
export function upload(file: object): Promise<UploadRes> {
    return request.post(`/upload`, file);
}


// 响应接口
export interface LoginUserRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 用户-登录
 * @param {string} acc
 * @param {string} pwd
 * @param {string} code
 * @returns
 */
export function loginUser(acc: string, pwd: string, code: string): Promise<LoginUserRes> {
    return request.post(`/userEntity/loginUser?acc=${acc}&pwd=${pwd}&code=${code}`);
}


