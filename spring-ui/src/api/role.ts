import request from "@/request";


// 参数接口
export interface UpdateRoleParams {
    id?: number;
    name?: string;
    power?: string;
    sequence?: number;
    status?: string;
    createBy?: string;
    createTime?: Record<string, unknown>;
    updateBy?: string;
    updateTime?: Record<string, unknown>;
    remark?: string;
}

// 响应接口
export interface UpdateRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 修改
 * @param {object} params $!{table.comment}
 * @param {number} params.id 角色编号
 * @param {string} params.name 名称
 * @param {string} params.power 权限字符
 * @param {number} params.sequence 显示顺序
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function updateRole(params: UpdateRoleParams): Promise<UpdateRoleRes> {
    return request.post(`/roleEntity/update`, params);
}


// 响应接口
export interface SelectRoleByNameRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-名称
 * @param {string} param
 * @returns
 */
export function selectRoleByName(param: string): Promise<SelectRoleByNameRes> {
    return request.post(`/roleEntity/selectRoleByName/${param}`);
}

// 响应接口
export interface SelectRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-编号
 * @param {string} param
 * @returns
 */
export function selectRole(param: number): Promise<SelectRoleRes> {
    return request.post(`/roleEntity/selectRole/${param}`);
}

// 响应接口
export interface ListPageRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 列表-分页
 * @param {string} page
 * @param {string} pageSize
 * @returns
 */
export function listPageRole(page: number, pageSize: number): Promise<ListPageRoleRes> {
    return request.post(`/roleEntity/listPage?page=${page}&pageSize=${pageSize}`);
}

// 参数接口
export interface InsertRoleParams {
    id?: number;
    name?: string;
    power?: string;
    sequence?: number;
    status?: string;
    createBy?: string;
    createTime?: Record<string, unknown>;
    updateBy?: string;
    updateTime?: Record<string, unknown>;
    remark?: string;
}

// 响应接口
export interface InsertRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 保存
 * @param {object} params $!{table.comment}
 * @param {number} params.id 角色编号
 * @param {string} params.name 名称
 * @param {string} params.power 权限字符
 * @param {number} params.sequence 显示顺序
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function insertRole(params: InsertRoleParams): Promise<InsertRoleRes> {
    return request.post(`/roleEntity/insert`, params);
}

// 响应接口
export interface ImportExcelRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 导入数据
 * @param {string} file
 * @returns
 */
export function importExcelRole(file: object): Promise<ImportExcelRoleRes> {
    return request.post(`/roleEntity/importExcel?file=${file}`);
}

// 响应接口
export interface ExportExcelRoleRes {}

/**
 * 导出数据
 * @returns
 */
export function exportExcelRole(): Promise<ExportExcelRoleRes> {
    return request.post(`/roleEntity/exportExcel`);
}

// 参数接口
export interface DeleteRoleParams {
    id?: number;
}

// 响应接口
export interface DeleteRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 删除
 * @param {object} params IdParam
 * @param {number} params.id ID，记录唯一标识
 * @returns
 */
export function deleteRole(params: DeleteRoleParams): Promise<DeleteRoleRes> {
    return request.post(`/roleEntity/delete`, params);
}


// 响应接口
export interface ListRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 列表
 * @returns
 */
export function listRole(): Promise<ListRoleRes> {
    return request.post(`/roleEntity/list`);
}
