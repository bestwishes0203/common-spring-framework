import request from "@/request";

// 参数接口
export interface UpdateDeptParams {
    id?: number;
    name?: string;
    ranks?: number;
    sort?: number;
    parent?: number;
    status?: string;
    createBy?: string;
    createTime?: string | unknown;
    updateBy?: string;
    updateTime?: string | unknown;
    remark?: string;
}

// 响应接口
export interface UpdateDeptRes {
    message: string;
    success: boolean;
    code: number;
    data: string | unknown;
}

/**
 * 修改
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {string} params.name 名称
 * @param {number} params.ranks 排序
 * @param {number} params.sort 类别
 * @param {number} params.parent 标识符
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function updateDept(params: UpdateDeptParams): Promise<UpdateDeptRes> {
    return request.post(`/deptEntity/update`, params);
}


// 响应接口
export interface SelectDeptByNameRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-名称
 * @param {string} param
 * @returns
 */
export function selectDeptByName(param: string): Promise<SelectDeptByNameRes> {
    return request.post(`/deptEntity/selectDeptByName/${param}`);
}

// 响应接口
export interface SelectDeptRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-编号
 * @param {string} param
 * @returns
 */
export function selectDept(param: number): Promise<SelectDeptRes> {
    return request.post(`/deptEntity/selectDept/${param}`);
}

// 响应接口
export interface ListDeptRes {
    message: string;
    success: boolean;
    code: number;
    data: string | unknown;
}

/**
 * 列表
 * @returns
 */
export function listDept(): Promise<ListDeptRes> {
    return request.post(`/deptEntity/list`);
}

// 响应接口
export interface ListPageDeptRes {
    message: string;
    success: boolean;
    code: number;
    data: string | unknown;
}

/**
 * 列表-分页
 * @param {string} page
 * @param {string} pageSize
 * @returns
 */
export function listPageDept(page: number, pageSize: number): Promise<ListPageDeptRes> {
    return request.post(`/deptEntity/listPage?page=${page}&pageSize=${pageSize}`);
}

// 参数接口
export interface InsertDeptParams {
    id?: number;
    name?: string;
    ranks?: number;
    sort?: number;
    parent?: number;
    status?: string;
    createBy?: string;
    createTime?: string | unknown;
    updateBy?: string;
    updateTime?: string | unknown;
    remark?: string;
}

// 响应接口
export interface InsertDeptRes {
    message: string;
    success: boolean;
    code: number;
    data: string | unknown;
}

/**
 * 保存
 * @param {object} params $!{table.comment}
 * @param {number} params.id 编号
 * @param {string} params.name 名称
 * @param {number} params.ranks 排序
 * @param {number} params.sort 类别
 * @param {number} params.parent 标识符
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function insertDept(params: InsertDeptParams): Promise<InsertDeptRes> {
    return request.post(`/deptEntity/insert`, params);
}

// 响应接口
export interface ImportExcelDeptRes {
    message: string;
    success: boolean;
    code: number;
    data: string | unknown;
}

/**
 * 导入数据
 * @param {string} file
 * @returns
 */
export function importExcelDept(file: object): Promise<ImportExcelDeptRes> {
    return request.post(`/deptEntity/importExcel?file=${file}`);
}

// 响应接口
export interface ExportExcelDeptRes {
}

/**
 * 导出数据
 * @returns
 */
export function exportExcelDept(): Promise<ExportExcelDeptRes> {
    return request.post(`/deptEntity/exportExcel`);
}

// 参数接口
export interface DeleteDeptParams {
    id?: number;
}

// 响应接口
export interface DeleteDeptRes {
    message: string;
    success: boolean;
    code: number;
    data: string | unknown;
}

/**
 * 删除
 * @param {object} params IdParam
 * @param {number} params.id ID，记录唯一标识
 * @returns
 */
export function deleteDept(params: DeleteDeptParams): Promise<DeleteDeptRes> {
    return request.post(`/deptEntity/delete`, params);
}




