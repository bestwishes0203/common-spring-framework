import request from "@/request";


// 参数接口
export interface UpdateUserRoleParams {
    id?: number;
    user?: number;
    role?: number;
    status?: string;
    createBy?: string;
    createTime?: string | unknown;
    updateBy?: string;
    updateTime?: string | unknown;
    remark?: string;
}

// 响应接口
export interface UpdateUserRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 修改
 * @param {object} params $!{table.comment}
 * @param {number} params.id
 * @param {number} params.user 用户编号
 * @param {number} params.role 角色编号
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function updateUserRole(params: UpdateUserRoleParams): Promise<UpdateUserRoleRes> {
    return request.post(`/userRoleEntity/update`, params);
}

// 响应接口
export interface SelectURByAccRes {
    message: string;
    success: boolean;
    code: number;
    data: string | unknown;
}

/**
 * 查询-用户
 * @param {string} param
 * @returns
 */
export function selectURByAcc(param: number): Promise<SelectURByAccRes> {
    return request.post(`/userRoleEntity/selectURByUser/${param}`);
}

// 响应接口
export interface SelectURByRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: string | unknown;
}

/**
 * 查询-角色
 * @param {string} param
 * @returns
 */
export function selectURByRole(param: number): Promise<SelectURByRoleRes> {
    return request.post(`/userRoleEntity/selectURByRole/${param}`);
}

// 响应接口
export interface ListPageUserRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 列表-分页
 * @param {string} page
 * @param {string} pageSize
 * @returns
 */
export function listPageUserRole(page: number, pageSize: number): Promise<ListPageUserRoleRes> {
    return request.post(`/userRoleEntity/listPage?page=${page}&pageSize=${pageSize}`);
}


// 参数接口
export interface InsertUserRoleParams {
    id?: number;
    user?: number;
    role?: number;
    status?: string;
    createBy?: string;
    createTime?: string | unknown;
    updateBy?: string;
    updateTime?: string | unknown;
    remark?: string;
}

// 响应接口
export interface InsertUserRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 保存
 * @param {object} params $!{table.comment}
 * @param {number} params.id
 * @param {number} params.user 用户编号
 * @param {number} params.role 角色编号
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function insertUserRole(params: InsertUserRoleParams): Promise<InsertUserRoleRes> {
    return request.post(`/userRoleEntity/insert`, params);
}

// 响应接口
export interface ImportExcelUserRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 导入数据
 * @param {string} file
 * @returns
 */
export function importExcelUserRole(file: object): Promise<ImportExcelUserRoleRes> {
    return request.post(`/userRoleEntity/importExcel?file=${file}`);
}

// 响应接口
export interface ExportExcelUserRoleRes {
}

/**
 * 导出数据
 * @returns
 */
export function exportExcelUserRole(): Promise<ExportExcelUserRoleRes> {
    return request.post(`/userRoleEntity/exportExcel`);
}

// 参数接口
export interface DeleteUserRoleParams {
    id?: number;
}

// 响应接口
export interface DeleteUserRoleRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 删除
 * @param {object} params IdParam
 * @param {number} params.id ID，记录唯一标识
 * @returns
 */
export function deleteUserRole(params: DeleteUserRoleParams): Promise<DeleteUserRoleRes> {
    return request.post(`/userRoleEntity/delete`, params);
}

