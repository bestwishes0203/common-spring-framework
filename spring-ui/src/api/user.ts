import request from "@/request";

// 参数接口
export interface UpdateUserParams {
    id?: number;
    name?: string;
    sex?: string;
    age?: number;
    pic?: string;
    acc?: string;
    pwd?: string;
    phone?: string;
    email?: string;
    dept?: string;
    post?: string;
    status?: string;
    createBy?: string;
    createTime?: string | unknown;
    updateBy?: string;
    updateTime?: string | unknown;
    remark?: string;
}

// 响应接口
export interface UpdateUserRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 修改-账号
 * @param {object} params $!{table.comment}
 * @param {number} params.id
 * @param {string} params.name 姓名
 * @param {string} params.sex 性别
 * @param {number} params.age 年龄
 * @param {string} params.pic 头像
 * @param {string} params.acc 账号
 * @param {string} params.pwd 密码
 * @param {string} params.phone 电话号码
 * @param {string} params.email 电子邮件
 * @param {string} params.dept 用户部门
 * @param {string} params.post 用户岗位
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function updateUser(params: UpdateUserParams): Promise<UpdateUserRes> {
    return request.post(`/userEntity/update`, params);
}

// 响应接口
export interface SelectUserByNameRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-姓名
 * @param {string} param
 * @returns
 */
export function selectUserByName(param: string): Promise<SelectUserByNameRes> {
    return request.post(`/userEntity/selectUserByName/${param}`);
}

// 响应接口
export interface SelectUserByAccRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 查询-账号
 * @param {string} param
 * @returns
 */
export function selectUserByAcc(param: string): Promise<SelectUserByAccRes> {
    return request.post(`/userEntity/selectUserByAcc/${param}`);
}


// 响应接口
export interface ListPageUserRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 列表-分页
 * @param {string} page
 * @param {string} pageSize
 * @returns
 */
export function listPageUser(page: number, pageSize: number): Promise<ListPageUserRes> {
    return request.post(`/userEntity/listPage?page=${page}&pageSize=${pageSize}`);
}

// 参数接口
export interface InsertUserParams {
    id?: number;
    name?: string;
    sex?: string;
    age?: number;
    pic?: string;
    acc?: string;
    pwd?: string;
    phone?: string;
    email?: string;
    dept?: string;
    post?: string;
    status?: string;
    createBy?: string;
    createTime?: string | unknown;
    updateBy?: string;
    updateTime?: string | unknown;
    remark?: string;
}

// 响应接口
export interface InsertUserRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 保存
 * @param {object} params $!{table.comment}
 * @param {number} params.id
 * @param {string} params.name 姓名
 * @param {string} params.sex 性别
 * @param {number} params.age 年龄
 * @param {string} params.pic 头像
 * @param {string} params.acc 账号
 * @param {string} params.pwd 密码
 * @param {string} params.phone 电话号码
 * @param {string} params.email 电子邮件
 * @param {string} params.dept 用户部门
 * @param {string} params.post 用户岗位
 * @param {string} params.status 状态（0正常 1停用）
 * @param {string} params.createBy 创建者
 * @param {object} params.createTime 创建时间
 * @param {string} params.updateBy 更新者
 * @param {object} params.updateTime 更新时间
 * @param {string} params.remark 备注
 * @returns
 */
export function insertUser(params: InsertUserParams): Promise<InsertUserRes> {
    return request.post(`/userEntity/insert`, params);
}

// 响应接口
export interface ImportExcelUserRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 导入数据
 * @param {string} file
 * @returns
 */
export function importExcelUser(file: object): Promise<ImportExcelUserRes> {
    return request.post(`/userEntity/importExcel?file=${file}`);
}

// 响应接口
export interface ExportExcelUserRes {
}

/**
 * 导出数据
 * @returns
 */
export function exportExcelUser(): Promise<ExportExcelUserRes> {
    return request.post(`/userEntity/exportExcel`);
}

// 参数接口
export interface DeleteUserParams {
    id?: number;
}

// 响应接口
export interface DeleteUserRes {
    message: string;
    success: boolean;
    code: number;
    data: Record<string, unknown>;
}

/**
 * 删除
 * @param {object} params IdParam
 * @param {number} params.id ID，记录唯一标识
 * @returns
 */
export function deleteUser(params: DeleteUserParams): Promise<DeleteUserRes> {
    return request.post(`/userEntity/delete`, params);
}

