import {defineStore} from "pinia";
import router from "@/router";

/**
 * 其内部可以包含，
 * 状态（state），
 * 操作（actions）,
 * getter（getters）
 */
export const index = defineStore("index", {
    state: () => ({
        //左侧导航栏是否折叠
        isCollapse: false,
        //默认选中的子菜单
        active: "0",
        //默认展开的菜单栏
        openeds: [],
        //存储tab标签页的信息
        tabsList: [{
            id: "0",
            name: "首页",
            path: "/home",
        }],
    }),
    actions: {
        collapseMenu() {
            //@ts-ignore
            this.isCollapse = !this.isCollapse;
        },
        initActive() {
            if (localStorage.getItem("active")) {
                //@ts-ignore
                this.active = localStorage.getItem("active")
            }
        },
        changeActive(e) {
            //@ts-ignore
            this.active = e
        },
        initTabs() {
            let tabsList = localStorage.getItem("TABS_LIST")
            if (tabsList) {
                //@ts-ignore
                this.tabsList = JSON.parse(tabsList);
            }
        },
        changeTabs(e) {
            //@ts-ignore
            const list = this.tabsList;
            list.forEach((item) => {
                if (item.id !== e.id) {
                    let result = list.findIndex((item) => item.id === e.id);
                    result === -1 ? list.push(e) : "";
                }
            });
            localStorage.setItem("TABS_LIST", JSON.stringify(list));
            //@ts-ignore
            this.active = e.id.toString();
            localStorage.setItem("active", e.id);
        },
        closeTabs(e) {
            //@ts-ignore
            const list = this.tabsList;
            //如果点击的菜单不是home，首先要遍历tabsList是否已经有菜单(name)，如果有删除
            let result = list.findIndex((item) => item.id === e.id);
            list.splice(result, 1);
            localStorage.setItem("TABS_LIST", JSON.stringify(list));
            //跳转到前一个
            router.push(list[list.length - 1].path);
            //记录前一个
            localStorage.setItem("active", list[list.length - 1].id);
            //菜单联动到前一个
            this.changeActive(list[list.length - 1].id);
        }
    },
});
