import {createRouter, createWebHistory} from 'vue-router'

const modules = import.meta.glob("../views/**/**.vue");

const children: any = [{
    path: '/home',
    name: 'home',
    component: () => import('../views/pages/HomeView.vue')
}];

const routes = [
    {
        path: '/',
        name: 'login',
        component: () => import('../views/LoginView.vue'),
    }, {
        path: '/main',
        name: 'layout',
        component: () => import('../views/LayoutView.vue'),
        redirect: '/home',
        children: children
    },
]

const childList = JSON.parse(localStorage.getItem("childList"));
if (childList !== null) {
    childList.map((item: any) => {
        const menuItem = {
            path: item.path,
            name: item.name,
            component: modules[`../views/pages/${item.component}View.vue`],
        };
        children.push(menuItem);
    });
}

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes,
})

export default router
