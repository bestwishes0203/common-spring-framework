# RBAC动态路由权限系统v1.0

## 概述

RBAC动态路由权限系统是一个基于现代Web技术的全功能应用，旨在为开发者提供高效、便捷的数据管理解决方案。该系统采用前后端分离架构，结合了多种先进技术，以确保系统的高性能和易用性。

## 项目结构

###### 根目录

```
根目录
    ├── mysql                     # 数据库脚本和相关文件
    ├── release                   # 发行的二进制文件和可执行程序
    ├── springboot                # 后端Spring Boot源代码
    ├── spring-ui                 # 前端Vue.js源代码
    ├── screenshot                # 系统截图和示例图片
    └── README.md                 # 项目介绍和安装指南
```

## 软件架构

洛可可白框架采用了分层架构设计，清晰地将业务逻辑、数据访问和用户界面分离。后端使用Spring Boot集成了RESTful API、安全性控制和事务管理等关键功能。前端则通过Vue.js实现了响应式用户界面和组件化开发。

## 安装教程

1. 确保你的开发环境中已安装Java 8或更高版本以及Node.js。
2. 克隆洛可可白框架的代码库到本地。
3. 在项目根目录下运行`mvn clean install`来构建后端。
4. 进入前端目录，运行`npm install`安装依赖，然后运行`npm run serve`启动开发服务器。
5. 访问`http://localhost:8080`查看应用。

## 使用说明

1. 使用Spring Security进行用户认证和授权。
2. 通过Vue Router管理页面路由和组件状态。
3. 利用Spring Data JPA进行数据持久化操作。
4. 使用Vuex进行状态管理，维护应用的全局状态。
5. 通过Element UI构建美观且响应式的用户界面。

## 技术栈

- **Node.js**: 作为后端开发环境，提供强大的异步处理能力。
- **NPM**: 用于管理项目依赖，简化开发流程。
- **Spring Boot**: 用于构建快速、易于部署的Java应用程序。
- **MySQL**: 关系型数据库，用于存储图书和用户数据。
- **Redis**: 高性能的键值对存储系统，用作缓存以提高系统响应速度。
- **Vue.js**: 前端框架，用于构建用户界面和单页应用程序。
- **Vite**: 现代化的前端构建工具，提供快速的冷启动和热模块替换。
- **IDEA**: 集成开发环境，用于编写和管理后端代码。
- **DataGrap**: 数据库管理工具，用于设计和管理MySQL数据库。
- **Webstorm**: 前端开发IDE，支持Vue.js等现代前端技术。
- **Apifox**: API开发工具，用于设计、测试和文档化API。
- **Another Redis Desktop Manager**: Redis数据库管理工具。
- **Navicat Premium 16**: 数据库开发工具，支持多种数据库。
- **Postman**: API测试工具，用于测试和文档化API。
- **Swagger**: 自动生成API文档的工具。
- **MyBatis**: 持久层框架，简化数据库操作。
- **ECharts**: 数据可视化库，用于生成图表。

## 功能亮点

- **前后端分离架构**: 采用现代化的前后端分离架构，使得前端和后端可以独立开发和部署。前端使用Vue.js框架，后端基于Spring Boot，这样的分离不仅提高了开发效率，还便于团队协作和系统维护。

- **动态路由权限管理**: 实现了基于角色的访问控制，确保不同权限的用户能看到不同的菜单和路由。管理员、图书馆员和普通用户都有定制化的界面和功能，提高了系统的安全性和用户体验。

- **数据导入导出**: 提供了便捷的数据操作功能，用户可以通过Excel表格批量导入图书信息，同时也能将借阅记录导出为Excel文件，方便进行数据分析和备份。

- **图表展示**: 利用ECharts库，系统能够生成各种图表，如柱状图、饼图等，直观地展示图书借阅情况、用户活跃度等关键数据。这些图表帮助管理员快速了解图书馆的运营状况，为决策提供数据支持。

- **验证码功能**: 在用户注册和登录时，系统会生成验证码，增加了一层安全防护，防止恶意攻击和自动化脚本的滥用。

- **图片上传**: 用户可以上传图书封面图片，系统支持图片的存储、展示和下载，提高了图书信息的丰富性和吸引力。

- **信息管理**: 提供了全面的图书信息管理功能，包括图书的添加、编辑、删除和查询。管理员可以轻松管理图书库存，更新图书状态。

- **后台管理**: 管理员可以通过后台管理界面，监控系统运行状态，管理用户账户，审核图书信息，以及进行系统设置和维护。

- **Swagger集成**: 后端API文档通过Swagger自动生成，提供了交互式的API文档，方便前后端开发者理解和使用API。

- **MyBatis框架**: 使用MyBatis框架简化数据库操作，提高了数据库交互的效率和可维护性。

- **Redis缓存**: 引入Redis作为缓存系统，提高了数据读取速度，优化了系统性能，尤其是在高并发场景下。

## 软件效果图

### 管理员账号效果

|                                                                                      |                                                                                      |
|--------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|
| ![屏幕截图](https://gitee.com/bestwishes0203/RBAC-Framework/raw/master/screenshot/1.jpg) | ![屏幕截图](https://gitee.com/bestwishes0203/RBAC-Framework/raw/master/screenshot/2.jpg) | 
| ![屏幕截图](https://gitee.com/bestwishes0203/RBAC-Framework/raw/master/screenshot/3.jpg) | ![屏幕截图](https://gitee.com/bestwishes0203/RBAC-Framework/raw/master/screenshot/4.jpg) | 
| ![屏幕截图](https://gitee.com/bestwishes0203/RBAC-Framework/raw/master/screenshot/5.jpg) | ![屏幕截图](https://gitee.com/bestwishes0203/RBAC-Framework/raw/master/screenshot/6.jpg) | 
| ![屏幕截图](https://gitee.com/bestwishes0203/RBAC-Framework/raw/master/screenshot/1.jpg) |                                                                                      |    



---

## 🚀 获取笔记

- **后端学习笔记**：[https://gitee.com/bestwishes0203/Front-end-notes](https://gitee.com/bestwishes0203/Front-end-notes)
- **前端学习笔记**：[https://gitee.com/bestwishes0203/Back-end-notes](https://gitee.com/bestwishes0203/Back-end-notes)

---

## 📌 联系方式

如果您对我们的项目感兴趣，或者有任何技术问题想要探讨，欢迎通过以下方式与我联系。我非常期待与您交流，共同学习，共同进步！🌊💡🤖

- **邮箱**：[2109664977@qq.com](mailto:2109664977@qq.com) 📧
- **Gitee**：[https://gitee.com/bestwishes0203](https://gitee.com/bestwishes0203) 🐱
- **GitHub**：[https://github.com/bestwishes0203](https://github.com/bestwishes0203) 🐙
- **CSDN**：[https://blog.csdn.net/interest_ing_/](https://blog.csdn.net/interest_ing_/) 📖
- **个人博客**：[http://bestwishes0203.github.io/blog/](http://bestwishes0203.github.io/blog/) 🏠

---

## 🎉 结语

感谢你的访问，期待与你在技术的道路上相遇！👋🌟🚀



