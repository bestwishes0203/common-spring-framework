use db_spring;

create table db_spring.sys_user
(
    user_id     int auto_increment
        primary key,
    user_name   varchar(50)             null comment '姓名',
    user_sex    varchar(1)              null comment '性别',
    user_age    int                     null comment '年龄',
    user_pic    varchar(255)            null comment '头像',
    user_acc    varchar(50)             null comment '账号',
    user_pwd    varchar(255)            null comment '密码',
    user_ide    char        default '0' null comment '角色（0用户 1管理员 2超级管理员）',
    status      char        default '0' null comment '状态（0正常 1停用）',
    create_by   varchar(64) default ''  null comment '创建者',
    create_time datetime                null comment '创建时间',
    update_by   varchar(64) default ''  null comment '更新者',
    update_time datetime                null comment '更新时间',
    remark      varchar(500)            null comment '备注'
)
    comment '用户表';

create table db_spring.sys_user_menu
(
    um_id       int auto_increment
        primary key,
    um_ide      int                     null comment '用户',
    um_menu     int                     null comment '菜单',
    status      char        default '0' null comment '状态（0正常 1停用）',
    create_by   varchar(64) default ''  null comment '创建者',
    create_time datetime                null comment '创建时间',
    update_by   varchar(64) default ''  null comment '更新者',
    update_time datetime                null comment '更新时间',
    remark      varchar(500)            null comment '备注'
)
    comment '用户-菜单表';


create table db_spring.sys_menu
(
    menu_id     int auto_increment
        primary key,
    menu_name   varchar(50)             null comment '名称',
    menu_intro  varchar(255)            null comment '介绍',
    menu_num    int                     null comment '排序',
    status      char        default '0' null comment '状态（0正常 1停用）',
    create_by   varchar(64) default ''  null comment '创建者',
    create_time datetime                null comment '创建时间',
    update_by   varchar(64) default ''  null comment '更新者',
    update_time datetime                null comment '更新时间',
    remark      varchar(500)            null comment '备注'
)
    comment '菜单表';

create table db_spring.sys_menu_route
(
    mr_id       int auto_increment
        primary key,
    mr_menu     int                     null comment '菜单',
    mr_route    int                     null comment '路由',
    status      char        default '0' null comment '状态（0正常 1停用）',
    create_by   varchar(64) default ''  null comment '创建者',
    create_time datetime                null comment '创建时间',
    update_by   varchar(64) default ''  null comment '更新者',
    update_time datetime                null comment '更新时间',
    remark      varchar(500)            null comment '备注'
)
    comment '菜单-路由表';


create table db_spring.sys_route
(
    route_id        int auto_increment
        primary key,
    route_name      varchar(50)             null comment '名称',
    route_path      varchar(255)            null comment '路径',
    route_component varchar(255)            null comment '组件',
    status          char        default '0' null comment '状态（0正常 1停用）',
    create_by       varchar(64) default ''  null comment '创建者',
    create_time     datetime                null comment '创建时间',
    update_by       varchar(64) default ''  null comment '更新者',
    update_time     datetime                null comment '更新时间',
    remark          varchar(500)            null comment '备注'
)
    comment '路由表';



INSERT INTO db_spring.sys_menu_route (mr_id, mr_menu, mr_route, status, create_by, create_time, update_by, update_time, remark) VALUES (1, 1, 1, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_menu_route (mr_id, mr_menu, mr_route, status, create_by, create_time, update_by, update_time, remark) VALUES (2, 1, 2, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_menu_route (mr_id, mr_menu, mr_route, status, create_by, create_time, update_by, update_time, remark) VALUES (3, 2, 3, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_menu_route (mr_id, mr_menu, mr_route, status, create_by, create_time, update_by, update_time, remark) VALUES (4, 2, 4, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_menu_route (mr_id, mr_menu, mr_route, status, create_by, create_time, update_by, update_time, remark) VALUES (5, 2, 5, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_menu_route (mr_id, mr_menu, mr_route, status, create_by, create_time, update_by, update_time, remark) VALUES (6, 3, 6, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_menu_route (mr_id, mr_menu, mr_route, status, create_by, create_time, update_by, update_time, remark) VALUES (7, 3, 7, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_menu (menu_id, menu_name, menu_intro, menu_num, status, create_by, create_time, update_by, update_time, remark) VALUES (1, '用户管理', null, null, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_menu (menu_id, menu_name, menu_intro, menu_num, status, create_by, create_time, update_by, update_time, remark) VALUES (2, '菜单管理', null, null, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_menu (menu_id, menu_name, menu_intro, menu_num, status, create_by, create_time, update_by, update_time, remark) VALUES (3, '个人中心', null, null, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_user_menu (um_id, um_ide, um_menu, status, create_by, create_time, update_by, update_time, remark) VALUES (1, 2, 1, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_user_menu (um_id, um_ide, um_menu, status, create_by, create_time, update_by, update_time, remark) VALUES (2, 2, 2, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_user_menu (um_id, um_ide, um_menu, status, create_by, create_time, update_by, update_time, remark) VALUES (3, 2, 3, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_user_menu (um_id, um_ide, um_menu, status, create_by, create_time, update_by, update_time, remark) VALUES (4, 1, 2, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_user_menu (um_id, um_ide, um_menu, status, create_by, create_time, update_by, update_time, remark) VALUES (5, 1, 3, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_user_menu (um_id, um_ide, um_menu, status, create_by, create_time, update_by, update_time, remark) VALUES (6, 0, 3, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_user (user_id, user_name, user_sex, user_age, user_pic, user_acc, user_pwd, user_ide, status, create_by, create_time, update_by, update_time, remark) VALUES (1, '1', null, null, null, '1', '1', '0', '0', '', null, '', null, null);
INSERT INTO db_spring.sys_user (user_id, user_name, user_sex, user_age, user_pic, user_acc, user_pwd, user_ide, status, create_by, create_time, update_by, update_time, remark) VALUES (2, '2', null, null, null, '2', '2', '1', '0', '', null, '', null, null);
INSERT INTO db_spring.sys_user (user_id, user_name, user_sex, user_age, user_pic, user_acc, user_pwd, user_ide, status, create_by, create_time, update_by, update_time, remark) VALUES (3, '3', null, null, null, '3', '3', '2', '0', '', null, '', null, null);
INSERT INTO db_spring.sys_route (route_id, route_name, route_path, route_component, status, create_by, create_time, update_by, update_time, remark) VALUES (1, '用户管理', null, null, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_route (route_id, route_name, route_path, route_component, status, create_by, create_time, update_by, update_time, remark) VALUES (2, '在线用户', null, null, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_route (route_id, route_name, route_path, route_component, status, create_by, create_time, update_by, update_time, remark) VALUES (3, '菜单管理', null, null, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_route (route_id, route_name, route_path, route_component, status, create_by, create_time, update_by, update_time, remark) VALUES (4, '路由管理', null, null, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_route (route_id, route_name, route_path, route_component, status, create_by, create_time, update_by, update_time, remark) VALUES (5, '菜单路由', null, null, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_route (route_id, route_name, route_path, route_component, status, create_by, create_time, update_by, update_time, remark) VALUES (6, '个人中心', null, null, '0', '', null, '', null, null);
INSERT INTO db_spring.sys_route (route_id, route_name, route_path, route_component, status, create_by, create_time, update_by, update_time, remark) VALUES (7, '修改信息', null, null, '0', '', null, '', null, null);

