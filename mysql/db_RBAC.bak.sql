/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80030 (8.0.30)
 Source Host           : localhost:3306
 Source Schema         : db_spring

 Target Server Type    : MySQL
 Target Server Version : 80030 (8.0.30)
 File Encoding         : 65001

 Date: 05/06/2024 09:58:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                             `dept_id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
                             `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
                             `dept_ranks` int NULL DEFAULT NULL COMMENT '排序',
                             `dept_sort` int NULL DEFAULT NULL COMMENT '类别',
                             `dept_parent` int NULL DEFAULT NULL COMMENT '标识符',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '状态（1正常 0停用）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门岗位表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, '总公司', NULL, NULL, 0, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dept` VALUES (2, '南昌分公司', NULL, NULL, 1, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dept` VALUES (3, '研发部门', NULL, NULL, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dept` VALUES (4, '测试部门', NULL, NULL, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dept` VALUES (5, '财务部门', NULL, NULL, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dept` VALUES (6, '运维部门', NULL, NULL, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dept` VALUES (7, '市场部门', NULL, NULL, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dept` VALUES (8, '长沙分公司', NULL, NULL, 1, '1', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
                             `dict_id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
                             `dict_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
                             `dict_field` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '字段',
                             `dict_data` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据',
                             `dict_mean` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '含义',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '状态（1正常 0停用）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '全局状态', 'status', '0', '禁用', '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dict` VALUES (2, '全局状态', 'status', '1', '正常', '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dict` VALUES (3, '性别', 'user_sex', '0', '女', '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dict` VALUES (4, '性别', 'user_sex', '1', '男', '1', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                             `menu_id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
                             `menu_icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
                             `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
                             `menu_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '路径',
                             `menu_component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组件',
                             `menu_ranks` int NULL DEFAULT NULL COMMENT '排序',
                             `menu_parent` int NULL DEFAULT NULL COMMENT '标识符',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '状态（1正常 0停用）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 'Monitor', '系统管理', NULL, NULL, NULL, 0, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2, 'Stopwatch', '系统监控', NULL, NULL, NULL, 0, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (3, 'Position', '系统工具', NULL, NULL, NULL, 0, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (4, 'User', '用户管理', '/user', 'User', NULL, 1, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (5, 'Postcard', '角色管理', '/role', 'Role', NULL, 1, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (6, 'Files', '菜单管理', '/menu', 'Menu', NULL, 1, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (7, 'Notification', '部门管理', '/dept', 'Dept', NULL, 1, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (8, 'MessageBox', '字典管理', '/dict', 'Dict', NULL, 1, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (9, 'Bell', '通知管理', '/notice', 'Notice', NULL, 1, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (10, 'TakeawayBox', '日志监控', '/log', 'Log', NULL, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (11, 'Service', '个人中心', NULL, NULL, NULL, 0, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (12, 'LocationInformation', '我的信息', '/myself', 'Myself', NULL, 11, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_menu` VALUES (13, 'Position', '岗位管理', '/post', 'Post', NULL, 1, '1', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
                             `post_id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
                             `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
                             `post_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '编码',
                             `post_ranks` int NULL DEFAULT NULL COMMENT '排序',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '状态（1正常 0停用）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '岗位表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, '董事长', 'ceo', NULL, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_post` VALUES (2, '项目经理', 'se', NULL, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_post` VALUES (3, '人力资源', 'hr', NULL, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_post` VALUES (4, '普通员工', 'user', NULL, '1', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `role_id` int NOT NULL AUTO_INCREMENT COMMENT '角色编号',
                             `role_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
                             `role_power` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限字符',
                             `role_sequence` int NULL DEFAULT NULL COMMENT '显示顺序',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '状态（1正常 0停用）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', NULL, NULL, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role` VALUES (2, '系统管理员', NULL, NULL, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role` VALUES (3, '普通管理员', NULL, NULL, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role` VALUES (4, '普通用户', NULL, NULL, '1', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                  `rm_id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
                                  `rm_role` int NULL DEFAULT NULL COMMENT '角色编号',
                                  `rm_menu` int NULL DEFAULT NULL COMMENT '菜单编号',
                                  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '状态（1正常 0停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`rm_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色-菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1, 1, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (3, 1, 3, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (4, 1, 4, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (5, 1, 5, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (6, 1, 6, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (7, 1, 7, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (8, 1, 8, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (9, 1, 9, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (10, 1, 10, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (11, 1, 11, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (12, 1, 12, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (13, 1, 13, '1', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `user_id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
                             `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '姓名',
                             `user_sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别',
                             `user_age` int NULL DEFAULT NULL COMMENT '年龄',
                             `user_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
                             `user_acc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账号',
                             `user_pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
                             `user_phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号码',
                             `user_email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电子邮箱',
                             `user_dept` int NULL DEFAULT NULL COMMENT '用户部门',
                             `user_post` int NULL DEFAULT NULL COMMENT '用户岗位',
                             `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '状态（1正常 0停用）',
                             `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, '邹子韬', '男', 24, '/src/assets/avatar/20240605093906664.png', '1', '1', '13592428614', 'zz314@icloud.com', 1, 1, '1', '邹子韬', '2001-05-26 16:52:11', '邹子韬', '2024-06-05 09:41:53', 'FC-DF-4034-F');
INSERT INTO `sys_user` VALUES (2, '吕岚', '男', 22, '/src/assets/avatar/1.png', '8874844503', '1842466317', '15811155047', 'lalu@icloud.com', 2, 4, '1', '吕岚', '2021-05-08 04:10:03', '邹子韬', '2024-04-09 23:10:39', '');
INSERT INTO `sys_user` VALUES (3, '龙震南', '男', 59, '/src/assets/avatar/1.png', '7290068753', '0400215503', '15811155047', 'lozhennan@icloud.com', 2, 4, '1', '龙震南', '2017-10-07 13:15:35', '龙震南', '2015-02-01 16:41:27', 'CE-DD-1926-C');
INSERT INTO `sys_user` VALUES (4, '何詩涵', '男', 61, '/src/assets/avatar/1.png', '2250720426', '6582080373', '18315916840', 'hesh@yahoo.com', 2, 4, '1', '何詩涵', '2020-08-24 11:31:56', '何詩涵', '2016-11-04 11:21:47', 'CF-AC-2106-Z');
INSERT INTO `sys_user` VALUES (5, '史璐', '男', 76, '/src/assets/avatar/20240605094326856.png', '7843577952', '7991880567', '15811155047', 'lushi@gmail.com', 2, 4, '1', '史璐', '2023-08-23 15:02:17', '邹子韬', '2024-06-05 09:43:29', 'AC-AE-6128-T');
INSERT INTO `sys_user` VALUES (6, '邓岚', '男', 79, '/src/assets/avatar/1.png', '2688309062', '9949949641', '18333621948', 'lande@mail.com', 2, 4, '1', '邓岚', '2022-10-02 16:45:43', '邓岚', '2016-05-09 14:21:05', 'BE-BD-1121-E');
INSERT INTO `sys_user` VALUES (7, '吕安琪', '男', 15, '/src/assets/avatar/1.png', '6760073770', '4657344089', '15009807100', 'lu1986@gmail.com', 2, 4, '1', '吕安琪', '2005-08-11 02:57:03', '吕安琪', '2017-06-16 14:23:57', 'BD-EE-6591-I');
INSERT INTO `sys_user` VALUES (8, '孙杰宏', '男', 94, '/src/assets/avatar/1.png', '3705094409', '9310455331', '15811155047', 'jiehongsun10@gmail.com', 2, 4, '1', '孙杰宏', '2005-05-12 01:14:19', '孙杰宏', '2009-08-30 13:43:20', 'CE-AA-2149-R');
INSERT INTO `sys_user` VALUES (9, '龙詩涵', '男', 20, '/src/assets/avatar/1.png', '0221651827', '3001196132', '15811155047', 'loshiha3@gmail.com', 2, 4, '1', '龙詩涵', '2010-03-28 04:19:53', '龙詩涵', '2018-07-23 22:59:39', 'AF-CF-3227-N');
INSERT INTO `sys_user` VALUES (10, '孔睿', '男', 58, '/src/assets/avatar/1.png', '5201509847', '9356875750', '13592428614', 'ruikong@yahoo.com', 2, 4, '1', '孔睿', '2005-12-15 21:43:26', '孔睿', '2015-03-04 05:08:25', 'FE-DA-9916-C');
INSERT INTO `sys_user` VALUES (11, '范云熙', '男', 21, '/src/assets/avatar/1.png', '9040894465', '2616865157', '13592428614', 'yunfa7@gmail.com', 2, 4, '1', '范云熙', '2012-08-10 09:19:19', '范云熙', '2009-11-06 01:08:44', 'CF-BA-2791-A');
INSERT INTO `sys_user` VALUES (12, '蔡岚', '男', 49, '/src/assets/avatar/1.png', '5323103712', '4098908143', '13592428614', 'lac1208@gmail.com', 2, 4, '1', '蔡岚', '2017-05-03 04:43:39', '蔡岚', '2010-09-02 15:59:37', 'BD-DA-3778-K');
INSERT INTO `sys_user` VALUES (13, '孟宇宁', '男', 65, '/src/assets/avatar/1.png', '6931894145', '3073480244', '15811155047', 'meng8@yahoo.com', 2, 4, '1', '孟宇宁', '2011-10-13 15:56:04', '孟宇宁', '2000-12-31 21:02:19', 'CB-EC-1897-F');
INSERT INTO `sys_user` VALUES (14, '叶杰宏', '男', 74, '/src/assets/avatar/1.png', '4934866445', '0607350642', '15811155047', 'yej6@gmail.com', 2, 4, '1', '叶杰宏', '2010-11-04 21:34:58', '叶杰宏', '2012-05-05 17:03:41', 'AD-AB-9136-P');
INSERT INTO `sys_user` VALUES (15, '唐安琪', '男', 67, '/src/assets/avatar/1.png', '2261693201', '6587359733', '19766143832', 'anqi1@hotmail.com', 2, 4, '1', '唐安琪', '2016-11-14 05:24:20', '唐安琪', '2018-08-19 19:13:25', 'AC-FF-7778-X');
INSERT INTO `sys_user` VALUES (16, '崔子异', '男', 42, '/src/assets/avatar/1.png', '1196341031', '6697183437', '14291273986', 'cuiziy@outlook.com', 2, 4, '1', '崔子异', '2007-09-21 10:13:14', '崔子异', '2018-04-18 17:00:06', 'DA-CE-6083-F');
INSERT INTO `sys_user` VALUES (17, '顾璐', '男', 59, '/src/assets/avatar/1.png', '4235070418', '2686407345', '18162514812', 'lug1@outlook.com', 2, 4, '1', '顾璐', '2011-08-17 08:44:41', '顾璐', '2013-10-04 08:08:52', 'CB-AE-6560-X');
INSERT INTO `sys_user` VALUES (18, '孟詩涵', '男', 26, '/src/assets/avatar/1.png', '1459932681', '0635225764', '19452148275', 'meng3@outlook.com', 2, 4, '1', '孟詩涵', '2009-10-25 04:52:24', '孟詩涵', '2022-10-25 00:11:39', 'FC-AF-9495-K');
INSERT INTO `sys_user` VALUES (19, '孔安琪', '男', 99, '/src/assets/avatar/1.png', '0279501974', '8016874000', '15002007358', 'konganqi@icloud.com', 2, 4, '1', '孔安琪', '2007-08-23 10:41:44', '孔安琪', '2015-12-18 23:43:39', 'EE-EA-9798-U');
INSERT INTO `sys_user` VALUES (20, '邹杰宏', '男', 27, '/src/assets/avatar/1.png', '2070311542', '7738564695', '19799225041', 'zjieh@gmail.com', 2, 4, '1', '邹杰宏', '2021-11-30 02:33:37', '邹杰宏', '2022-05-24 18:52:35', 'EC-BB-0854-V');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
                                  `ur_id` int NOT NULL AUTO_INCREMENT,
                                  `ur_user` int NULL DEFAULT NULL COMMENT '用户编号',
                                  `ur_role` int NULL DEFAULT NULL COMMENT '角色编号',
                                  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '状态（1正常 0停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`ur_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色-菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 18, 4, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (2, 8, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (3, 14, 4, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (4, 4, 3, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (5, 3, 3, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (6, 5, 4, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (7, 9, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (8, 11, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (9, 17, 3, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (10, 12, 2, '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (11, 1, 1, '1', '', NULL, '', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
